package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskShowByIndexResponse extends AbstractTaskResponse {

    public TaskShowByIndexResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskShowByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
