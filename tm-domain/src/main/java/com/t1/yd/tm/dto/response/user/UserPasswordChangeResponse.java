package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.model.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserPasswordChangeResponse extends AbstractUserResponse {

    public UserPasswordChangeResponse(@Nullable final User user) {
        super(user);
    }

    public UserPasswordChangeResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
