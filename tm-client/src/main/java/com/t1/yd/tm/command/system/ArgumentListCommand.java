package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show available arguments";

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");

        for (final String arg : getArgumentList()) {
            System.out.println(arg);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    public List<String> getArgumentList() {
        @NotNull final List<String> args = new ArrayList<>();

        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();

        for (@NotNull final ICommand command : commands) {
            @Nullable final String arg = command.getArgument();
            if (arg == null || arg.isEmpty()) continue;
            args.add(arg);
        }

        return args;
    }

}
