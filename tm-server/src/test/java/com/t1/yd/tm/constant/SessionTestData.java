package com.t1.yd.tm.constant;

import com.t1.yd.tm.model.Session;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.t1.yd.tm.constant.UserTestData.USER1;

@UtilityClass
public class SessionTestData {

    @NotNull
    public final static String SESSION_NAME = "SESSION_NAME";

    @NotNull
    public final static String SESSION_DESCRIPTION = "SESSION_DESCRIPTION";

    @NotNull
    public final static Session SESSION1 = new Session();

    @NotNull
    public final static Session SESSION2 = new Session();

    @NotNull
    public final static List<Session> ALL_SESSIONS = Arrays.asList(SESSION1, SESSION2);

    static {
        SESSION1.setUserId(USER1.getId());
        SESSION1.setDate(new Date());
    }

}

