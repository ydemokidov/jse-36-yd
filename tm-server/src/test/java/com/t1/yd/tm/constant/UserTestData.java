package com.t1.yd.tm.constant;

import com.t1.yd.tm.model.User;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public class UserTestData {

    @NotNull
    public static final User USER1 = new User();

    @NotNull
    public static final String USER1_LOGIN = "user1";

    @NotNull
    public static final String USER1_EMAIL = "user1@mail.ru";

    @NotNull
    public static final User ADMIN = new User();

    @NotNull
    public static final String ADMIN_LOGIN = "admin";

    @NotNull
    public static final String ADMIN_EMAIL = "admin@mail.ru";

    @NotNull
    public static final List<User> ALL_USERS = Arrays.asList(USER1, ADMIN);

    static {
        USER1.setLogin(USER1_LOGIN);
        USER1.setEmail(USER1_EMAIL);

        ADMIN.setLogin(ADMIN_LOGIN);
        ADMIN.setEmail(ADMIN_EMAIL);
    }

}
