package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.marker.UnitCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    private IUserRepository repository;

    @Before
    public void initRepository() {
        repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void add() {
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
    }

    @Test
    public void addAll() {
        repository.add(ALL_USERS);
        Assert.assertEquals(ALL_USERS.size(), repository.findAll().size());
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
    }


    @Test
    public void clear() {
        repository.add(ALL_USERS);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findByLogin() {
        repository.add(USER1);
        repository.add(ADMIN);
        Assert.assertEquals(USER1, repository.findByLogin(USER1_LOGIN));
    }

    @Test
    public void findByEmail() {
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findByEmail(USER1_EMAIL));
    }

    @Test
    public void existsByLogin() {
        repository.add(USER1);
        Assert.assertTrue(repository.isLoginExist(USER1_LOGIN));
        Assert.assertFalse(repository.isLoginExist(ADMIN_LOGIN));
    }

    @Test
    public void existsByEmail() {
        repository.add(USER1);
        Assert.assertTrue(repository.isEmailExist(USER1_EMAIL));
        Assert.assertFalse(repository.isEmailExist(ADMIN_EMAIL));
    }

}
