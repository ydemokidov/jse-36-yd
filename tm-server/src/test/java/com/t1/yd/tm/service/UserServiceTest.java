package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.marker.UnitCategory;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    private IUserRepository repository;

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    private IUserService service;

    private ISaltProvider saltProvider = new PropertyService();

    @Before
    public void initRepository() {
        repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());

        projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.findAll().isEmpty());

        taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.findAll().isEmpty());

        service = new UserService(repository, projectRepository, taskRepository, saltProvider);
    }

    @Test
    public void add() {
        service.add(USER1);
        Assert.assertEquals(USER1, service.findOneById(USER1.getId()));
    }

    @Test
    public void addAll() {
        service.add(ALL_USERS);
        Assert.assertEquals(ALL_USERS.size(), service.findAll().size());
        Assert.assertEquals(USER1, service.findOneById(USER1.getId()));
    }


    @Test
    public void clear() {
        service.add(ALL_USERS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findByLogin() {
        service.add(USER1);
        service.add(ADMIN);
        Assert.assertEquals(USER1, service.findByLogin(USER1_LOGIN));
    }

    @Test
    public void findByEmail() {
        service.add(USER1);
        Assert.assertEquals(USER1, service.findByEmail(USER1_EMAIL));
    }

    @Test
    public void existsByLogin() {
        service.add(USER1);
        Assert.assertTrue(service.isLoginExist(USER1_LOGIN));
        Assert.assertFalse(service.isLoginExist(ADMIN_LOGIN));
    }

    @Test
    public void existsByEmail() {
        service.add(USER1);
        Assert.assertTrue(service.isEmailExist(USER1_EMAIL));
        Assert.assertFalse(service.isEmailExist(ADMIN_EMAIL));
    }

    @Test
    public void lockByLogin() {
        service.add(USER1);
        service.lockByLogin(USER1_LOGIN);
        @NotNull final User tmpUser = service.findByLogin(USER1_LOGIN);
        Assert.assertTrue(tmpUser.getLocked());
    }

    @Test
    public void unlockByLogin() {
        service.add(USER1);
        service.lockByLogin(USER1_LOGIN);
        service.unlockByLogin(USER1_LOGIN);
        @NotNull final User tmpUser = service.findByLogin(USER1_LOGIN);
        Assert.assertFalse(tmpUser.getLocked());
    }

    @Test
    public void removeByLogin() {
        service.add(USER1);
        service.removeByLogin(USER1_LOGIN);
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void removeByEmail() {
        service.add(USER1);
        service.removeByEmail(USER1_EMAIL);
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void updateUser() {
        @NotNull final String lastName = "lastname";
        @NotNull final String fstName = "fstname";
        @NotNull final String midName = "midname";
        service.add(USER1);
        service.updateUser(USER1.getId(), fstName, lastName, midName);
        Assert.assertEquals(lastName, service.findOneById(USER1.getId()).getLastName());
        Assert.assertEquals(fstName, service.findOneById(USER1.getId()).getFirstName());
        Assert.assertEquals(midName, service.findOneById(USER1.getId()).getMiddleName());
    }

}
