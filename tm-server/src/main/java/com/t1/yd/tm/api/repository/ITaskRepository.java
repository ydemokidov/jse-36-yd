package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
