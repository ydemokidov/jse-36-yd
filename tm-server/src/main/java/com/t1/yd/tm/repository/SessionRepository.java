package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ISessionRepository;
import com.t1.yd.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
