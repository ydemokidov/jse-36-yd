package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.*;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Task findTaskById(@NotNull String userId, @NotNull String id) {
        @Nullable Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task findTaskByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task removeTaskById(@NotNull String userId, @NotNull String id) {
        @Nullable Task task = removeById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task removeTaskByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable Task task = removeByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task(userId, name, description);
        return add(task);
    }

    @NotNull
    @Override
    public Task updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @NotNull final Task task = findTaskById(userId, id);

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @NotNull final Task task = findTaskByIndex(userId, index);

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final Task task = findTaskById(userId, id);
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        @NotNull final Task task = findTaskByIndex(userId, index);
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

}