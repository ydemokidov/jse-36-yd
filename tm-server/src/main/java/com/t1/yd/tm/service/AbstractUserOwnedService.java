package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IUserOwnedRepository;
import com.t1.yd.tm.api.service.IUserOwnedService;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<E extends AbstractUserOwnedEntity, R extends IUserOwnedRepository<E>> extends AbstractService<E, R> implements IUserOwnedService<E> {

    @NotNull
    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @NotNull
    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.add(userId, entity);
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator comparator) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId, comparator);
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final List<E> userEntities = findAll(userId);
        if (index < 0 || index >= userEntities.size()) throw new IndexIncorrectException();

        return repository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final List<E> userEntities = findAll(userId);
        if (index < 0 || index >= userEntities.size()) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return findAll(userId).size();
    }

}
